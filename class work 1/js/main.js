let clientsArray = [];
let employeesArray = [];

// Create buttons >>>>>>>>>>>
let buttonAdd = document.createElement("button");
let buttonDelete = document.createElement("button");
let buttonList = document.createElement("button");

buttonAdd.innerHTML = "Add client";
buttonDelete.innerHTML = "Delete client";
buttonList.innerHTML = "List clients";

buttonList.classList.add("list");
buttonDelete.classList.add("delete");
buttonAdd.classList.add("add");

document.body.prepend(buttonList);
document.body.prepend(buttonDelete);
document.body.prepend(buttonAdd);


// Add listeners

buttonAdd.addEventListener("click", () => {
    let name = prompt("name");
    let surname = prompt("surname");
    let newPerson = new Person({name, surname});
    listManager.addRecord(clientsArray, newPerson);
});
buttonDelete.addEventListener("click", () => {
    let name = prompt("hame to delete");
    listManager.deleteRecord(clientsArray, name);
});
buttonList.addEventListener("click", () => {
    listManager.listRecords(clientsArray, document.querySelector(".listOutput"));
});

// <<<<<<<<<<<<<<<<<<<<<<
